package test;

import commons.BrowserLaunch;
import commons.MaxProductValue;
import commonsinterface.BrowserLaunchInterface;
import org.testng.annotations.Test;
import pages.AmazonHomePage;
import pages.FlipKartHomePage;
import properties.PropertiesFile;

public class EcommerceAssignment {

    BrowserLaunchInterface browserLaunchInterface = new BrowserLaunch();

    @Test
    public void fetchTheBestPriceForIphone(){
        FlipKartHomePage flipKartHomePage = new FlipKartHomePage();
        AmazonHomePage amazonHomePage = new AmazonHomePage();
        MaxProductValue maxProductValue = new MaxProductValue();
        PropertiesFile propertiesFile = new PropertiesFile();
        double amazon = amazonHomePage.amazonPage(browserLaunchInterface
            .launchBrowserUsingwebDriverManager(propertiesFile.getBrowserProperties(),
                propertiesFile.getAmazonProperties()));

        double flipKart = flipKartHomePage.flipKartPage(browserLaunchInterface
            .launchBrowserUsingwebDriverManager(propertiesFile.getBrowserProperties(),
                propertiesFile.getFlipKartProperties()));

        maxProductValue.getProductValue(amazon, flipKart);
    }
}
