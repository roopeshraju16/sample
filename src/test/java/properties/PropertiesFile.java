package properties;

import commons.BrowserLaunch;
import commons.ErrorMessage;
import commonsinterface.BrowserLaunchInterface;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesFile {

    BrowserLaunchInterface browserLaunchInterface = new BrowserLaunch();

    public String getBrowserProperties() {
        String browserName = null;
        try {
            Properties prop = new Properties();
            InputStream inputStream = new FileInputStream(
                browserLaunchInterface.getProjectLocation() + File.separator + "src"
                    + File.separator + "test" + File.separator + "resources" + File.separator
                    + "config.properties");
            prop.load(inputStream);

            browserName = prop.getProperty("browserName");
        } catch (Exception e) {
            throw new RuntimeException(ErrorMessage.UNABLE_TO_SET_PROPERTIES, e);
        }
        return browserName;
    }

    public String getAmazonProperties() {
        String browserName = null;
        try {
            Properties prop = new Properties();
            InputStream inputStream = new FileInputStream(
                browserLaunchInterface.getProjectLocation() + File.separator + "src"
                    + File.separator + "test" + File.separator + "resources" + File.separator
                    + "config.properties");
            prop.load(inputStream);
            browserName = prop.getProperty("amazonEcommercePlayerName");
        } catch (Exception e) {
            throw new RuntimeException(ErrorMessage.UNABLE_TO_SET_PROPERTIES, e);
        }
        return browserName;
    }

    public String getFlipKartProperties() {
        String browserName = null;
        try {
            Properties prop = new Properties();
            InputStream inputStream = new FileInputStream(
                browserLaunchInterface.getProjectLocation() + File.separator + "src"
                    + File.separator + "test" + File.separator + "resources" + File.separator
                    + "config.properties");
            prop.load(inputStream);
            browserName = prop.getProperty("flipKartEcommercePlayerName");
        } catch (Exception e) {
            throw new RuntimeException(ErrorMessage.UNABLE_TO_SET_PROPERTIES, e);
        }
        return browserName;
    }

}
