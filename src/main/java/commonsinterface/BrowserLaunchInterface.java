package commonsinterface;

import org.openqa.selenium.WebDriver;

public interface BrowserLaunchInterface {
    //For to fetch the operatingSystem
    public String getOperatingSystem();

    //For to fetch the projectLocation
    public String getProjectLocation();

    //For to launch the firefoxbrowser
    public void  fireFoxBrowserLaunch(String ecommercePlayerName);

    //For to launch the chromebrowser
    public void  chromeBrowserLaunch(String ecommercePlayerName);

    //For to setup the browser and ecommercePlayerName to run the automation
    public void setBrowserName(String browserName, String ecommercePlayerName);

    //For to fetch which commerce player to open
    public String getEcommerceUrl(String ecommercePlayerName);

    //For to launch the browser using webDriverManager
    public WebDriver launchBrowserUsingwebDriverManager(String browserName, String ecommercePlayerName);
    
}
