package commonsinterface;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public interface PageObject {
    public WebElement fetchLocatorForObject(WebDriver driver, String xpath);

    public void clickOnBtn(WebElement element);

    public String getText(WebDriver driver, String xpath);
}
