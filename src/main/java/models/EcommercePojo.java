package models;

public class EcommercePojo {
    private String ecommerceUrl;
    private String ecommercePlayerName;

    public String getEcommercePlayerName() {
        return ecommercePlayerName;
    }

    public void setEcommercePlayerName(String ecommercePlayerName) {
        this.ecommercePlayerName = ecommercePlayerName;
    }

    public String getEcommerceUrl() {
        return ecommerceUrl;
    }

    public void setEcommerceUrl(String ecommerceUrl) {
        this.ecommerceUrl = ecommerceUrl;
    }
}
