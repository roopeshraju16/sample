package models;

public class ProductsPojo {
    private String ecommercePlayer;
    private String productName;

    public String getEcommercePlayer() {
        return ecommercePlayer;
    }

    public void setEcommercePlayer(String ecommercePlayer) {
        this.ecommercePlayer = ecommercePlayer;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
