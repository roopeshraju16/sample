package commons;

import commonsinterface.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PageObjectImpl implements PageObject {

    public WebElement fetchLocatorForObject(WebDriver driver, String xpath) {
        WebElement crossButton = driver.findElement(By.xpath(xpath));
        return crossButton;
    }

    public void clickOnBtn(WebElement element) {
        element.click();
    }

    public String getText(WebDriver driver, String xpath) {
        return driver.findElement(By.xpath(xpath)).getText();
    }
}
