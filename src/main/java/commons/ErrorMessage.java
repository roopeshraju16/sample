package commons;

public class ErrorMessage {
    public static String UNABLE_FETCH_ECOMMERCE_URL="Unable to fetch the ecommerceUrl";

    public static String UNABLE_FETCH_PRODUCT_NAMES="Unable to fetch the productNames";

    public static String UNABLE_TO_SET_PROPERTIES="Unable to set values from properties file";

    public static String UNABLE_TO_FETCH_AMAZON_COST="Unable to fetch the amazon cost";

    public static String UNABLE_TO_FETCH_FLIPKART_COST="Unable to fetch the flipKart cost";
}
