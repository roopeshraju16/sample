package commons;

public class MaxProductValue {

    public void getProductValue(double iphoneYellowAmazonPrice, double iphoneYellowFlipKartPrice) {
        double minimumValue = Math.min(iphoneYellowAmazonPrice, iphoneYellowFlipKartPrice);
        if (minimumValue == iphoneYellowAmazonPrice) {
            System.out.println("In amazon website iphone(64GB) yellow colour has lower price:  "
                + iphoneYellowAmazonPrice);
        } else if (minimumValue == iphoneYellowFlipKartPrice) {
            System.out.println(
                "In this flipKart website iphone(64GB) yellow colour has lower price:  "
                    + iphoneYellowFlipKartPrice);
        }
    }
}
