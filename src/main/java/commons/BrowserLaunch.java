package commons;

import commonsinterface.BrowserLaunchInterface;
import io.github.bonigarcia.wdm.WebDriverManager;
import models.EcommercePojo;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.util.List;
import java.util.Properties;

public class BrowserLaunch implements BrowserLaunchInterface {

    //For to run fetch the operating system
    public String getOperatingSystem() {
        return System.getProperty("os.name");
    }

    //For to fetch the project location
    public String getProjectLocation() {
        return System.getProperty("user.dir");
    }

    public void fireFoxBrowserLaunch(String ecommercePlayerName) {
        WebDriver driver = new FirefoxDriver();
        if (getOperatingSystem().equalsIgnoreCase("Mac OS X")) {
            System.setProperty("webdriver.gecko.driver",
                getProjectLocation() + File.separator + "src" + File.separator + "test"
                    + File.separator + "resources" + File.separator + "geckodriver" + File.separator
                    + "geckodriver");
            driver.get(ecommercePlayerName);
        } else if (getOperatingSystem().startsWith("Windows")) {
            System.setProperty("webdriver.gecko.driver",
                getProjectLocation() + File.separator + "src" + File.separator + "test"
                    + File.separator + "resources" + File.separator + "windowsgeckdriver"
                    + File.separator + "geckodriver.exe");
            driver.get(ecommercePlayerName);
        }
    }

    public void chromeBrowserLaunch(String ecommercePlayerName) {
        WebDriver driver = new ChromeDriver();
        if (getOperatingSystem().equalsIgnoreCase("Mac OS X")) {
            System.setProperty("webdriver.chrome.driver",
                getProjectLocation() + File.separator + "src" + File.separator + "test"
                    + File.separator + "resources" + File.separator + "chromedriver"
                    + File.separator + "chromedriver");
            driver.get(ecommercePlayerName);
        } else if (getOperatingSystem().startsWith("Windows")) {
            System.setProperty("webdriver.chrome.driver",
                getProjectLocation() + File.separator + "src" + File.separator + "test"
                    + File.separator + "resources" + File.separator + "windowschromedriver"
                    + File.separator + "chromedriver.exe");
            driver.get(ecommercePlayerName);
        }
    }


    public void setBrowserName(String browserName, String ecommercePlayerName) {
        if (browserName.equalsIgnoreCase("fireFox")) {
            fireFoxBrowserLaunch(ecommercePlayerName);
        } else if (browserName.equalsIgnoreCase("chrome")) {
            chromeBrowserLaunch(ecommercePlayerName);
        }
    }

    public WebDriver launchBrowserUsingwebDriverManager(String browserName, String ecommercePlayerName) {
        WebDriver driver = null;
        if (browserName.equalsIgnoreCase("fireFox")) {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
            driver.get(getEcommerceUrl(ecommercePlayerName));
        } else if (browserName.equalsIgnoreCase("chrome")) {
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();
            driver.get(getEcommerceUrl(ecommercePlayerName));
        }
        return driver;
    }

    public String getEcommerceUrl(String ecommercePlayerName) {
        CsvFileHandler csvFileHandler = new CsvFileHandler();
        String ecommerceUrl = null;
        try {
            List<EcommercePojo> ecommercePojoList = csvFileHandler.loadCsvFromFile(new File(
                getProjectLocation() + File.separator + "src" + File.separator + "test"
                    + File.separator + "resources" + File.separator + "testdata" + File.separator
                    + "urls.csv"), EcommercePojo.class);
            for (EcommercePojo ecommercePojo : ecommercePojoList) {
                if (ecommercePojo.getEcommercePlayerName().equalsIgnoreCase(ecommercePlayerName)) {
                    ecommerceUrl = ecommercePojo.getEcommerceUrl();
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(ErrorMessage.UNABLE_FETCH_ECOMMERCE_URL, e);
        }
        return ecommerceUrl;
    }
}
