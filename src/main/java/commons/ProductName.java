package commons;

import commonsinterface.BrowserLaunchInterface;
import models.ProductsPojo;

import java.io.File;
import java.util.List;

public class ProductName {

    BrowserLaunchInterface browserLaunchInterface = new BrowserLaunch();
    public String getProductName(int indexValue){
        String productName=null;
        try {
            List<ProductsPojo> productsPojoList= CsvFileHandler.loadCsvFromFile(new File(
                browserLaunchInterface.getProjectLocation() + File.separator + "src"
                    + File.separator + "test" + File.separator + "resources" + File.separator
                    + "testdata" + File.separator + "products.csv"), ProductsPojo.class);
             productName=productsPojoList.get(indexValue).getProductName();
        }catch (Exception e){
        throw new RuntimeException(ErrorMessage.UNABLE_FETCH_PRODUCT_NAMES,e);
        }
        return productName;
    }
}
