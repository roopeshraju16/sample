package pages;

import commons.ErrorMessage;
import commons.PageObjectImpl;
import commons.ProductName;
import commonsinterface.PageObject;
import org.openqa.selenium.WebDriver;
import java.util.Iterator;
import java.util.Set;

public class FlipKartHomePage {

    ProductName productName = new ProductName();

    String productPriceOfFlipKart = null;

    String flipKartCrossBtn = "/html/body/div[2]/div/div/button";

    String flipKartSearchBox = "//input[@placeholder='Search for products, brands and more']";

    String flipKartSearchBtn =
        "//body/div[@id='container']/div/div[@class='_3ybBIU']/div[@class='_1tz-RS']/div[@class='_3pNZKl']/div[@class='Y5-ZPI']/form[@class='_1WMLwI header-form-search']/div[@class='col-12-12 _2tVp4j']/button[@class='vh79eN']/*[1]";

    String flipKartProductName = "//div[@class='_3wU53n']";

    String flipKartPrice = "//div[@class='_1vC4OE _3qQ9m1']";

    PageObject pageObject = new PageObjectImpl();

    public double flipKartPage(WebDriver driver){
        double iphoneYellowFlipKartPrice = 0;
        try {
            pageObject.clickOnBtn(pageObject.fetchLocatorForObject(driver, flipKartCrossBtn));
            pageObject.fetchLocatorForObject(driver, flipKartSearchBox)
                .sendKeys(productName.getProductName(0));
            pageObject.clickOnBtn(pageObject.fetchLocatorForObject(driver, flipKartSearchBtn));
            Thread.sleep(5000);
            String productValue = pageObject.getText(driver, flipKartProductName);
            if (productValue.startsWith("Apple iPhone XR")) {
                pageObject.clickOnBtn(pageObject.fetchLocatorForObject(driver, flipKartProductName));
            } else {
                throw new RuntimeException();
            }
            Set<String> windowIds = driver.getWindowHandles();
            Iterator<String> iterator = windowIds.iterator();
            String parentWindow = iterator.next();
            String childWindow = iterator.next();
            driver.switchTo().window(childWindow);
            Thread.sleep(5000);
            productPriceOfFlipKart = pageObject.getText(driver, flipKartPrice);
            Thread.sleep(5000);
            driver.quit();
            String flipKartPrice = productPriceOfFlipKart.replaceAll("₹", "");
            iphoneYellowFlipKartPrice = Double.parseDouble(flipKartPrice.replace(",", ""));
            System.out.println("flipKart Price:  "+iphoneYellowFlipKartPrice);
        }catch (Exception e){
          throw new RuntimeException(ErrorMessage.UNABLE_TO_FETCH_FLIPKART_COST,e);
        }
        return iphoneYellowFlipKartPrice;
    }
}
