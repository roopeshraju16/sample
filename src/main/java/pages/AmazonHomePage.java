package pages;

import commons.ErrorMessage;
import commons.PageObjectImpl;
import commons.ProductName;
import commonsinterface.PageObject;
import org.openqa.selenium.WebDriver;

import java.util.Iterator;
import java.util.Set;

public class AmazonHomePage {
    ProductName productName = new ProductName();

    PageObject pageObject = new PageObjectImpl();

    String amazonProductPrice = null;

    String amazonSearchBox = "//input[@id='twotabsearchtextbox']";

    String amazonSearchBtn = "//input[@value='Go']";

    String amazonProductValue = "//span[@id='priceblock_ourprice']";

    public double amazonPage(WebDriver driver) {
        double iphoneYellowAmazonPrice = 0;
        try {
            pageObject.fetchLocatorForObject(driver, amazonSearchBox)
                .sendKeys(productName.getProductName(0));
            pageObject.clickOnBtn(pageObject.fetchLocatorForObject(driver, amazonSearchBtn));
            String productValue = pageObject.getText(driver,
                "//span[contains(text(),'Apple " + productName.getProductName(0) + "')]");
            if (productValue.contains("Apple " + productName.getProductName(0))) {
                pageObject.clickOnBtn(pageObject.fetchLocatorForObject(driver,
                    "//span[contains(text(),'Apple " + productName.getProductName(0) + "')]"));
            } else {
                throw new RuntimeException();
            }
            Set<String> windowIds = driver.getWindowHandles();
            Iterator<String> iterator = windowIds.iterator();
            String parentWindow = iterator.next();
            String childWindow = iterator.next();
            driver.switchTo().window(childWindow);
            Thread.sleep(5000);
            amazonProductPrice = pageObject.getText(driver, amazonProductValue);
            Thread.sleep(5000);
            driver.quit();
            String amazonPrice = amazonProductPrice.replaceAll("₹", "");
            iphoneYellowAmazonPrice = Double.parseDouble(amazonPrice.replace(",", ""));
            System.out.println("amazonPrice:  "+iphoneYellowAmazonPrice);

        } catch (Exception e) {
            throw new RuntimeException(ErrorMessage.UNABLE_TO_FETCH_AMAZON_COST, e);
        }
        return iphoneYellowAmazonPrice;
    }
}

